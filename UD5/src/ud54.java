import javax.swing.JOptionPane;

public class ud54 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
 
  	/* 4) Haz una aplicaci�n que calcule el �rea de un circulo (pi*R2). El radio se pedir� por teclado
		recuerda pasar de String a double con Double.parseDouble). Usa la constante PI y el m�todo pow de Math. */ 
		
		
		//usuario introduce el radio
		String inRadio= JOptionPane.showInputDialog("Introduce el radio");

		//string pasa a double
		double radio = Double.parseDouble(inRadio);
		
		//calculo pi*(Radio * 2)
		double area = Math.PI*Math.pow(radio, 2);
		
		System.out.println("EL area es igual a "+area);
	
	}

}
