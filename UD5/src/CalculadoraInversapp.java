import javax.swing.JOptionPane;
public class CalculadoraInversapp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		

		int num1;
        int num2;
        int resultado=0;
 
        String txtNum1=JOptionPane.showInputDialog("Escribe el primer numero");
        num1=Integer.parseInt(txtNum1);
        
        String operacion=JOptionPane.showInputDialog("Escribe el codigo de operacion");

        
        String txtNum2=JOptionPane.showInputDialog("Escribe el segundo numero");
        num2=Integer.parseInt(txtNum2);

 
        switch (operacion){
            case "+":
                resultado=num1+num2;
                break;
            case "-":
                resultado=num1-num2;
                break;
            case "*":
                resultado=num1*num2;
                break;
            case "/":
                resultado=num1/num2;
                break;
            case "^":
                resultado=(int)Math.pow(num1, num2);
                break;
            case "%":
                resultado=num1%num2;
                break;
        }
 
        JOptionPane.showMessageDialog(null, num1+" "+operacion+" "+num2+" = "+resultado);

	}

}
